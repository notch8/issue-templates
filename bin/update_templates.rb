# frozen_string_literal: true

# This will create a new commit with the updated feature templates.
require 'gitlab'
require 'dotenv'

Dotenv.load
g = Gitlab.client(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV['GITLAB_API_PRIVATE_TOKEN'],
  httparty: {
    headers: { 'Cookie' => 'gitlab_canary=true' }
  }
)
raise ArgumentError, 'no project specified' if ARGV[0].nil?

project = ARGV[0]
source = 'main'
branch = 'update-issue-templates'
message = 'Updates the issue/MR templates'
actions = []
old_template_files = %w[
  Bug.md
  Feature.md
  Question.md
]
current_template_files = %w[Issue.md]
all_template_files = old_template_files + current_template_files
template_paths = %w[
  .gitlab/issue_templates/
  .gitlab/merge_request_templates/
]

# update current templates if they exist, delete all old templates
all_template_files.each do |filename|
  template_paths.each do |path|
    g.get_file(project, path + filename, source) # throws Gitlab::Error::NotFound if file does not exists
    action = old_template_files.include?(filename) ? 'delete' : 'update'
    actions << { file_path: path + filename, action: action }
  rescue Gitlab::Error::NotFound => e
    puts e.message
    next
  end
end

# create current template files if they do not exist
current_template_files.each do |filename|
  next if actions.collect { |h| h[:file_path].include?(filename) }.any?

  template_paths.each do |path|
    actions << { file_path: path + filename, action: 'create', content: File.new(path + filename).read }
  end
end

commit = g.create_commit(project, branch, message, actions, start_branch: source)
puts commit.to_hash

title = 'Update issue templates'
description = 'Replaces the gitlab issue templates with the newest versions'
mr = g.create_merge_request(project, title, source_branch: branch, target_branch: source, description: description, remove_source_branch: true)
puts mr.to_hash
