# Notch8 Issue Templates

These are the default issue templates for Notch8. For more information about how to use them, check the playbook links listed at the bottom of each of them.

# Updating the templates

If you end up updating the templates, be sure to check the update_templates.rb script to make sure that necessary changes are made.

You can manually update the templates in a repo by just copying the `.gitlab` folder into the root directory of a project, or you can run the [update_templates.rb](bin/update_templates.rb) script.

## Using the script

1. `git pull` to make sure you have the most recent version of the templates
2. `bundle install`
3. Set your access token in .env (in GitLab: Settings > Access Tokens, make sure it has access to the API scope)
4. `ruby bin/update_templates.rb "notch8/<repository-name>"` in the root directory of this repo (e.g. for this project, the `<repository-name>` would be `notch8/issue-templates`)
